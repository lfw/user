<?php

namespace Weblab\User\Record;

use Pckg\Database\Record;
use Weblab\User\Entity\Users;

class User extends Record
{

    protected $entity = Users::class;

}