<?php

namespace Weblab\User\Entity;

use Pckg\Database\Entity;
use Weblab\User\Record\User;

class Users extends Entity
{

    protected $record = User::class;

}